package io.sample.order.facade.event.listener.domain;

import io.naraway.janitor.event.EventHandler;
import io.sample.order.flow.productbrief.domain.logic.ProductBriefHandlerLogic;
import io.naraway.product.event.product.ProductDomainEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class ProductDataEventHandler {
    //
    private final ProductBriefHandlerLogic productHandlerLogic;

    @EventHandler
    public void handle(ProductDomainEvent event) {
        //
        productHandlerLogic.onProduct(event);
    }
}
