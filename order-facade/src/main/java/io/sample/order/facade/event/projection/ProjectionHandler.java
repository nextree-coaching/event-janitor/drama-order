/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.facade.event.projection;

import io.sample.order.aggregate.order.domain.logic.OrderLogic;
import io.naraway.accent.domain.message.event.broker.StreamEventMessage;
import io.sample.order.aggregate.order.domain.event.OrderEvent;
import io.sample.order.aggregate.ProductBrief.domain.event.ProductBriefEvent;
import io.sample.order.aggregate.ProductBrief.domain.logic.ProductBriefLogic;

public class ProjectionHandler {
    private final OrderLogic orderLogic; // Autogen by nara studio
    private final ProductBriefLogic productBriefLogic;

    public ProjectionHandler(OrderLogic orderLogic, ProductBriefLogic productBriefLogic) {
        /* Autogen by nara studio */
        this.orderLogic = orderLogic;
        this.productBriefLogic = productBriefLogic;
    }

    public void handle(StreamEventMessage streamEventMessage) {
        /* Autogen by nara studio */
        String classFullName = streamEventMessage.getPayloadClass();
        String payload = streamEventMessage.getPayload();
        String eventName = classFullName.substring(classFullName.lastIndexOf(".") + 1);
        switch(eventName) {
            case "OrderEvent":
                OrderEvent orderEvent = OrderEvent.fromJson(payload);
                orderLogic.handleEventForProjection(orderEvent);
                break;
            case "ProductBriefEvent":
                ProductBriefEvent productBriefEvent = ProductBriefEvent.fromJson(payload);
                productBriefLogic.handleEventForProjection(productBriefEvent);
                break;
        }
    }
}
