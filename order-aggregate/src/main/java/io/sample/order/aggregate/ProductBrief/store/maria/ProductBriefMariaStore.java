/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.store.maria;

import io.sample.order.aggregate.ProductBrief.store.ProductBriefStore;
import io.sample.order.aggregate.ProductBrief.store.maria.jpo.ProductBriefJpo;
import io.sample.order.aggregate.ProductBrief.store.maria.repository.ProductBriefMariaRepository;
import org.springframework.stereotype.Repository;
import io.sample.order.aggregate.ProductBrief.domain.entity.ProductBrief;

import java.util.Optional;
import org.springframework.data.domain.Pageable;
import io.naraway.accent.domain.type.Offset;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Repository
public class ProductBriefMariaStore implements ProductBriefStore {
    /* Autogen by nara studio */
    private final ProductBriefMariaRepository productBriefMariaRepository;

    public ProductBriefMariaStore(ProductBriefMariaRepository productBriefMariaRepository) {
        /* Autogen by nara studio */
        this.productBriefMariaRepository = productBriefMariaRepository;
    }

    @Override
    public void create(ProductBrief productBrief) {
        /* Autogen by nara studio */
        ProductBriefJpo productBriefJpo = new ProductBriefJpo(productBrief);
        productBriefMariaRepository.save(productBriefJpo);
    }

    @Override
    public ProductBrief retrieve(String id) {
        /* Autogen by nara studio */
        Optional<ProductBriefJpo> productBriefJpo = productBriefMariaRepository.findById(id);
        return productBriefJpo.map(ProductBriefJpo::toDomain).orElse(null);
    }

    @Override
    public void update(ProductBrief productBrief) {
        /* Autogen by nara studio */
        ProductBriefJpo productBriefJpo = new ProductBriefJpo(productBrief);
        productBriefMariaRepository.save(productBriefJpo);
    }

    @Override
    public void delete(ProductBrief productBrief) {
        /* Autogen by nara studio */
        productBriefMariaRepository.deleteById(productBrief.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        productBriefMariaRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return productBriefMariaRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
