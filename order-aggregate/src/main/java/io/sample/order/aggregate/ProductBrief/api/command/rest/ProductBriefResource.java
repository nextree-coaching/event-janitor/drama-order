/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.api.command.rest;

import io.sample.order.aggregate.ProductBrief.api.command.command.ProductBriefCommand;
import io.sample.order.aggregate.ProductBrief.domain.logic.ProductBriefLogic;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.naraway.accent.domain.message.api.command.CommandResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/aggregate/ProductBrief")
public class ProductBriefResource implements ProductBriefFacade {
    /* Autogen by nara studio */
    private final ProductBriefLogic productBriefLogic;

    public ProductBriefResource(ProductBriefLogic productBriefLogic) {
        /* Autogen by nara studio */
        this.productBriefLogic = productBriefLogic;
    }

    @Override
    @PostMapping("/product-brief/command")
    public CommandResponse executeProductBrief(@RequestBody ProductBriefCommand productBriefCommand) {
        /* Autogen by nara studio */
        return productBriefLogic.routeCommand(productBriefCommand).getCommandResponse();
    }
}
