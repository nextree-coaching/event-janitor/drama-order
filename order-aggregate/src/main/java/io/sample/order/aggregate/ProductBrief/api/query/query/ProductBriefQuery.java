/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.api.query.query;

import io.sample.order.aggregate.ProductBrief.store.ProductBriefStore;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.query.CqrsBaseQuery;
import io.sample.order.aggregate.ProductBrief.domain.entity.ProductBrief;

@Getter
@Setter
@NoArgsConstructor
public class ProductBriefQuery extends CqrsBaseQuery<ProductBrief> {
    /* Autogen by nara studio */
    private String productBriefId;

    public void execute(ProductBriefStore productBriefStore) {
        /* Autogen by nara studio */
        setQueryResult(productBriefStore.retrieve(productBriefId));
    }
}
