/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.domain.event;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.event.CqrsDataEvent;
import io.sample.order.aggregate.ProductBrief.domain.entity.ProductBrief;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.message.event.CqrsDataEventType;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class ProductBriefEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private ProductBrief productBrief;
    private String productBriefId;
    private NameValueList nameValues;

    protected ProductBriefEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static ProductBriefEvent newProductBriefRegisteredEvent(ProductBrief productBrief, String productBriefId) {
        /* Autogen by nara studio */
        ProductBriefEvent event = new ProductBriefEvent(CqrsDataEventType.Registered);
        event.setProductBrief(productBrief);
        event.setProductBriefId(productBriefId);
        return event;
    }

    public static ProductBriefEvent newProductBriefModifiedEvent(String productBriefId, NameValueList nameValues, ProductBrief productBrief) {
        /* Autogen by nara studio */
        ProductBriefEvent event = new ProductBriefEvent(CqrsDataEventType.Modified);
        event.setProductBriefId(productBriefId);
        event.setNameValues(nameValues);
        event.setProductBrief(productBrief);
        return event;
    }

    public static ProductBriefEvent newProductBriefRemovedEvent(ProductBrief productBrief, String productBriefId) {
        /* Autogen by nara studio */
        ProductBriefEvent event = new ProductBriefEvent(CqrsDataEventType.Removed);
        event.setProductBrief(productBrief);
        event.setProductBriefId(productBriefId);
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static ProductBriefEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, ProductBriefEvent.class);
    }
}
