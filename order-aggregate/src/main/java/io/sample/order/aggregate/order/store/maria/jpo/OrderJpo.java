/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.store.maria.jpo;

import io.naraway.accent.domain.type.IdName;
import io.sample.order.aggregate.order.domain.entity.Order;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import io.naraway.accent.store.jpa.StageEntityJpo;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "ORDERX")
public class OrderJpo extends StageEntityJpo {
    //
    private String productId; //
    private String productName; //
    private int quantity;
    private boolean canceled;

    public OrderJpo(Order order) {
        /* Autogen by nara studio */
        super(order);
        BeanUtils.copyProperties(order, this);
        this.productId = order.getProduct().getId();
        this.productName = order.getProduct().getName();
    }

    public Order toDomain() {
        /* Autogen by nara studio */
        Order order = new Order(getId(), genActorKey());
        BeanUtils.copyProperties(this, order);
        IdName idName = new IdName();
        idName.setId(productId);
        idName.setName(productName);
        order.setProduct(idName);
        return order;
    }

    public static List<Order> toDomains(List<OrderJpo> orderJpos) {
        /* Autogen by nara studio */
        return orderJpos.stream().map(OrderJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static OrderJpo sample() {
        /* Autogen by nara studio */
        return new OrderJpo(Order.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample());
    }
}
