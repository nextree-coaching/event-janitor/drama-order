/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.api.query.rest;

import io.sample.order.aggregate.ProductBrief.store.ProductBriefStore;
import io.sample.order.aggregate.ProductBrief.store.maria.jpo.ProductBriefJpo;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.naraway.accent.util.query.RdbQueryRequest;

import javax.persistence.EntityManager;
import io.naraway.accent.domain.message.api.query.QueryResponse;
import io.sample.order.aggregate.ProductBrief.domain.entity.ProductBrief;
import io.sample.order.aggregate.ProductBrief.api.query.query.ProductBriefQuery;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import io.sample.order.aggregate.ProductBrief.api.query.query.ProductBriefDynamicQuery;
import java.util.List;
import io.sample.order.aggregate.ProductBrief.api.query.query.ProductBriefsDynamicQuery;

@RestController
@RequestMapping("/aggregate/ProductBrief/product-brief/query")
public class ProductBriefQueryResource implements ProductBriefQueryFacade {
    //
    private final ProductBriefStore productBriefStore;
    private final RdbQueryRequest<ProductBriefJpo> request;

    public ProductBriefQueryResource(ProductBriefStore productBriefStore, EntityManager entityManager) {
        /* Autogen by nara studio */
        this.productBriefStore = productBriefStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QueryResponse<ProductBrief> execute(@RequestBody ProductBriefQuery productBriefQuery) {
        /* Autogen by nara studio */
        productBriefQuery.execute(productBriefStore);
        return productBriefQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-single")
    public QueryResponse<ProductBrief> execute(@RequestBody ProductBriefDynamicQuery productBriefDynamicQuery) {
        /* Autogen by nara studio */
        productBriefDynamicQuery.execute(request);
        return productBriefDynamicQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QueryResponse<List<ProductBrief>> execute(@RequestBody ProductBriefsDynamicQuery productBriefsDynamicQuery) {
        /* Autogen by nara studio */
        productBriefsDynamicQuery.execute(request);
        return productBriefsDynamicQuery.getQueryResponse();
    }
}
