/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.store;

import io.sample.order.aggregate.ProductBrief.domain.entity.ProductBrief;

public interface ProductBriefStore {
    /* Autogen by nara studio */
    void create(ProductBrief productBrief);
    ProductBrief retrieve(String id);
    void update(ProductBrief productBrief);
    void delete(ProductBrief productBrief);
    void delete(String id);
    boolean exists(String id);
}
