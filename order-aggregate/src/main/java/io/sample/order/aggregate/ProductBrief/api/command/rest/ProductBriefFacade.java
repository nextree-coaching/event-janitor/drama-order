/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.api.command.rest;

import io.naraway.accent.domain.message.api.command.CommandResponse;
import io.sample.order.aggregate.ProductBrief.api.command.command.ProductBriefCommand;

public interface ProductBriefFacade {
    /* Autogen by nara studio */
    CommandResponse executeProductBrief(ProductBriefCommand productBriefCommand);
}
