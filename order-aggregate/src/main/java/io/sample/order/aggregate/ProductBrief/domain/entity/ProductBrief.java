/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.domain.entity;

import io.naraway.accent.domain.ddd.GenOptions;

import io.sample.order.aggregate.ProductBrief.domain.entity.sdo.ProductBriefCdo;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import io.naraway.accent.domain.ddd.StageEntity;
import io.naraway.accent.domain.ddd.DomainAggregate;
import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import org.springframework.beans.BeanUtils;
import io.naraway.accent.domain.type.NameValue;

/*
ex)
@GenerateOptions(
    updatable = { "name" },
    properties = {
        @RdbProperty(name = "name", columnName = "col_name", columnType = "varchar(255)", columnNullable = true)
    })
*/
@Getter
@Setter
@NoArgsConstructor
@GenOptions
public class ProductBrief extends StageEntity implements DomainAggregate {
    //
    private String name;
    private String briefView;
    private int quantity;

    public ProductBrief(String id, ActorKey actorKey) {
        //
        super(id, actorKey);
    }

    public ProductBrief(ProductBriefCdo productBriefCdo) {
        //
        super(productBriefCdo.getActorKey());
        BeanUtils.copyProperties(productBriefCdo, this);
    }

    public static ProductBrief newInstance(ProductBriefCdo productBriefCdo, NameValueList nameValueList) {
        //
        ProductBrief productBrief = new ProductBrief(productBriefCdo);
        productBrief.modifyAttributes(nameValueList);
        return productBrief;
    }

    public String toString() {
        //
        return toJson();
    }

    public static ProductBrief fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ProductBrief.class);
    }

    public static ProductBrief sample() {
        //
        return new ProductBrief(ProductBriefCdo.sample());
    }

    @Override
    protected void modifyAttributes(NameValueList nameValues) {
        //
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch(nameValue.getName()) {
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public NameValueList genNameValueList() {
        //
        NameValueList nameValueList = NameValueList.newEmptyInstance();
        return nameValueList;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
