/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.domain.entity.sdo;

import io.naraway.accent.domain.type.IdName;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import io.naraway.accent.util.json.JsonSerializable;
import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderCdo implements JsonSerializable {
    //
    private ActorKey actorKey;
    private IdName product;
    private int quantity;


    public String toString() {
        //
        return toJson();
    }

    public static OrderCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, OrderCdo.class);
    }

    public static OrderCdo sample() {
        //
        return new OrderCdo();
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
