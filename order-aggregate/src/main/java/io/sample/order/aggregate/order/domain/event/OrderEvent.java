/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.domain.event;

import io.sample.order.aggregate.order.domain.entity.Order;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.event.CqrsDataEvent;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.message.event.CqrsDataEventType;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class OrderEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private Order order;
    private String orderId;
    private NameValueList nameValues;

    protected OrderEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static OrderEvent newOrderRegisteredEvent(Order order, String orderId) {
        /* Autogen by nara studio */
        OrderEvent event = new OrderEvent(CqrsDataEventType.Registered);
        event.setOrder(order);
        event.setOrderId(orderId);
        return event;
    }

    public static OrderEvent newOrderModifiedEvent(String orderId, NameValueList nameValues, Order order) {
        /* Autogen by nara studio */
        OrderEvent event = new OrderEvent(CqrsDataEventType.Modified);
        event.setOrderId(orderId);
        event.setNameValues(nameValues);
        event.setOrder(order);
        return event;
    }

    public static OrderEvent newOrderRemovedEvent(Order order, String orderId) {
        /* Autogen by nara studio */
        OrderEvent event = new OrderEvent(CqrsDataEventType.Removed);
        event.setOrder(order);
        event.setOrderId(orderId);
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static OrderEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, OrderEvent.class);
    }
}
