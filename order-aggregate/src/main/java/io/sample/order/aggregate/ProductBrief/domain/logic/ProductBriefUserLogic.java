package io.sample.order.aggregate.ProductBrief.domain.logic;

import io.sample.order.aggregate.ProductBrief.domain.entity.ProductBrief;
import io.sample.order.aggregate.ProductBrief.store.ProductBriefStore;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductBriefUserLogic {
    //
    private final ProductBriefLogic productViewLogic;
    private final ProductBriefStore productViewStore;

    public int increaseStock(String productId, int quantity) {
        //
        ProductBrief productView = productViewLogic.findProductBrief(productId);
        int curQuantity = productView.getQuantity() + quantity;
        productView.setQuantity(curQuantity);
        productViewStore.update(productView);

        return curQuantity;
    }

    public int decreaseStock(String productId, int quantity) {
        //
        ProductBrief productBrief = productViewLogic.findProductBrief(productId);
        int curQuantity = productBrief.getQuantity() - quantity;
        productBrief.setQuantity(curQuantity);
        productViewStore.update(productBrief);

        return curQuantity;
    }
}
