/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.store.maria.repository;

import io.sample.order.aggregate.ProductBrief.store.maria.jpo.ProductBriefJpo;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductBriefMariaRepository extends PagingAndSortingRepository<ProductBriefJpo, String> {
    /* Autogen by nara studio */
}
