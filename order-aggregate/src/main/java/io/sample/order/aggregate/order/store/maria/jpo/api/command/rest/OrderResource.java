/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.store.maria.jpo.api.command.rest;

import io.sample.order.aggregate.order.store.maria.jpo.api.command.command.OrderCommand;
import io.sample.order.aggregate.order.domain.logic.OrderLogic;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.naraway.accent.domain.message.api.command.CommandResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/aggregate/order")
public class OrderResource implements OrderFacade {
    /* Autogen by nara studio */
    private final OrderLogic orderLogic;

    public OrderResource(OrderLogic orderLogic) {
        /* Autogen by nara studio */
        this.orderLogic = orderLogic;
    }

    @Override
    @PostMapping("/order/command")
    public CommandResponse executeOrder(@RequestBody OrderCommand orderCommand) {
        /* Autogen by nara studio */
        return orderLogic.routeCommand(orderCommand).getCommandResponse();
    }
}
