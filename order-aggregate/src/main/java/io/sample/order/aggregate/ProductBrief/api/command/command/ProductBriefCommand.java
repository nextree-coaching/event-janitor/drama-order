/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.api.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.command.CqrsBaseCommand;
import io.sample.order.aggregate.ProductBrief.domain.entity.sdo.ProductBriefCdo;
import java.util.List;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.message.api.command.CqrsBaseCommandType;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class ProductBriefCommand extends CqrsBaseCommand {
    //
    private ProductBriefCdo productBriefCdo;
    private List<ProductBriefCdo> productBriefCdos;
    private boolean multiCdo;
    private String productBriefId;
    private NameValueList nameValues;

    protected ProductBriefCommand(CqrsBaseCommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static ProductBriefCommand newRegisterProductBriefCommand(ProductBriefCdo productBriefCdo) {
        /* Autogen by nara studio */
        ProductBriefCommand command = new ProductBriefCommand(CqrsBaseCommandType.Register);
        command.setProductBriefCdo(productBriefCdo);
        return command;
    }

    public static ProductBriefCommand newRegisterProductBriefCommand(List<ProductBriefCdo> productBriefCdos) {
        /* Autogen by nara studio */
        ProductBriefCommand command = new ProductBriefCommand(CqrsBaseCommandType.Register);
        command.setProductBriefCdos(productBriefCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static ProductBriefCommand newModifyProductBriefCommand(String productBriefId, NameValueList nameValues) {
        /* Autogen by nara studio */
        ProductBriefCommand command = new ProductBriefCommand(CqrsBaseCommandType.Modify);
        command.setProductBriefId(productBriefId);
        command.setNameValues(nameValues);
        return command;
    }

    public static ProductBriefCommand newRemoveProductBriefCommand(String productBriefId) {
        /* Autogen by nara studio */
        ProductBriefCommand command = new ProductBriefCommand(CqrsBaseCommandType.Remove);
        command.setProductBriefId(productBriefId);
        return command;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static ProductBriefCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, ProductBriefCommand.class);
    }

    public static ProductBriefCommand sampleForRegister() {
        /* Autogen by nara studio */
        return newRegisterProductBriefCommand(ProductBriefCdo.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sampleForRegister());
    }
}
