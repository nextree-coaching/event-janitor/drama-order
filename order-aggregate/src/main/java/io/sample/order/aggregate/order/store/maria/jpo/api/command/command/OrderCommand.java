/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.store.maria.jpo.api.command.command;

import io.sample.order.aggregate.order.domain.entity.sdo.OrderCdo;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.command.CqrsBaseCommand;

import java.util.List;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.message.api.command.CqrsBaseCommandType;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class OrderCommand extends CqrsBaseCommand {
    //
    private OrderCdo orderCdo;
    private List<OrderCdo> orderCdos;
    private boolean multiCdo;
    private String orderId;
    private NameValueList nameValues;

    protected OrderCommand(CqrsBaseCommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static OrderCommand newRegisterOrderCommand(OrderCdo orderCdo) {
        /* Autogen by nara studio */
        OrderCommand command = new OrderCommand(CqrsBaseCommandType.Register);
        command.setOrderCdo(orderCdo);
        return command;
    }

    public static OrderCommand newRegisterOrderCommand(List<OrderCdo> orderCdos) {
        /* Autogen by nara studio */
        OrderCommand command = new OrderCommand(CqrsBaseCommandType.Register);
        command.setOrderCdos(orderCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static OrderCommand newModifyOrderCommand(String orderId, NameValueList nameValues) {
        /* Autogen by nara studio */
        OrderCommand command = new OrderCommand(CqrsBaseCommandType.Modify);
        command.setOrderId(orderId);
        command.setNameValues(nameValues);
        return command;
    }

    public static OrderCommand newRemoveOrderCommand(String orderId) {
        /* Autogen by nara studio */
        OrderCommand command = new OrderCommand(CqrsBaseCommandType.Remove);
        command.setOrderId(orderId);
        return command;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static OrderCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, OrderCommand.class);
    }

    public static OrderCommand sampleForRegister() {
        /* Autogen by nara studio */
        return newRegisterOrderCommand(OrderCdo.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sampleForRegister());
    }
}
