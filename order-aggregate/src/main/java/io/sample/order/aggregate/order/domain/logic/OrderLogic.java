/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.domain.logic;

import io.sample.order.aggregate.order.domain.entity.Order;
import io.sample.order.aggregate.order.domain.entity.sdo.OrderCdo;
import io.sample.order.aggregate.order.domain.event.OrderEvent;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import io.sample.order.aggregate.order.store.OrderStore;
import io.naraway.janitor.EventStream;
import io.sample.order.aggregate.order.store.maria.jpo.api.command.command.OrderCommand;
import io.naraway.accent.domain.message.api.command.CommandResponse;
import io.naraway.accent.domain.message.api.FailureMessage;
import java.util.Optional;

import io.naraway.accent.domain.type.NameValueList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.NoSuchElementException;

@Service
@Transactional
public class OrderLogic {
    //
    private final OrderStore orderStore;
    private final EventStream eventStream;

    public OrderLogic(OrderStore orderStore, EventStream eventStream) {
        /* Autogen by nara studio */
        this.orderStore = orderStore;
        this.eventStream = eventStream;
    }

    public OrderCommand routeCommand(OrderCommand command) {
        /* Autogen by nara studio */
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = this.registerOrders(command.getOrderCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = Optional.ofNullable(command.getNameValues()).filter(nameValueList -> command.getNameValues().size() != 0).map(nameValueList -> this.registerOrder(command.getOrderCdo(), nameValueList)).orElse(this.registerOrder(command.getOrderCdo()));
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyOrder(command.getOrderId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getOrderId()));
                break;
            case Remove:
                this.removeOrder(command.getOrderId());
                command.setCommandResponse(new CommandResponse(command.getOrderId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerOrder(OrderCdo orderCdo) {
        /* Autogen by nara studio */
        Order order = new Order(orderCdo);
        if (orderStore.exists(order.getId())) {
            throw new IllegalArgumentException("order already exists. " + order.getId());
        }
        orderStore.create(order);
        OrderEvent orderEvent = OrderEvent.newOrderRegisteredEvent(order, order.getId());
        eventStream.publishEvent(orderEvent);
        return order.getId();
    }

    public String registerOrder(OrderCdo orderCdo, NameValueList nameValueList) {
        /* Autogen by nara studio */
        Order order = Order.newInstance(orderCdo, nameValueList);
        if (orderStore.exists(order.getId())) {
            throw new IllegalArgumentException("order already exists. " + order.getId());
        }
        orderStore.create(order);
        OrderEvent orderEvent = OrderEvent.newOrderRegisteredEvent(order, order.getId());
        eventStream.publishEvent(orderEvent);
        return order.getId();
    }

    public List<String> registerOrders(List<OrderCdo> orderCdos) {
        /* Autogen by nara studio */
        return orderCdos.stream().map(orderCdo -> this.registerOrder(orderCdo)).collect(Collectors.toList());
    }

    public Order findOrder(String orderId) {
        /* Autogen by nara studio */
        Order order = orderStore.retrieve(orderId);
        if (order == null) {
            throw new NoSuchElementException("Order id: " + orderId);
        }
        return order;
    }

    public void modifyOrder(String orderId, NameValueList nameValues) {
        /* Autogen by nara studio */
        Order order = findOrder(orderId);
        order.modify(nameValues);
        orderStore.update(order);
        OrderEvent orderEvent = OrderEvent.newOrderModifiedEvent(orderId, nameValues, order);
        eventStream.publishEvent(orderEvent);
    }

    public void removeOrder(String orderId) {
        /* Autogen by nara studio */
        Order order = findOrder(orderId);
        orderStore.delete(order);
        OrderEvent orderEvent = OrderEvent.newOrderRemovedEvent(order, order.getId());
        eventStream.publishEvent(orderEvent);
    }

    public boolean existsOrder(String orderId) {
        /* Autogen by nara studio */
        return orderStore.exists(orderId);
    }

    public void handleEventForProjection(OrderEvent orderEvent) {
        /* Autogen by nara studio */
        switch(orderEvent.getCqrsDataEventType()) {
            case Registered:
                orderStore.create(orderEvent.getOrder());
                break;
            case Modified:
                Order order = orderStore.retrieve(orderEvent.getOrderId());
                order.modify(orderEvent.getNameValues());
                orderStore.update(order);
                break;
            case Removed:
                orderStore.delete(orderEvent.getOrderId());
                break;
        }
    }
}
