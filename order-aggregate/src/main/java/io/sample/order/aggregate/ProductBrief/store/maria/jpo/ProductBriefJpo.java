/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.store.maria.jpo;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import io.naraway.accent.store.jpa.StageEntityJpo;
import io.sample.order.aggregate.ProductBrief.domain.entity.ProductBrief;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "PRODUCT_BRIEF")
public class ProductBriefJpo extends StageEntityJpo {
    //

    public ProductBriefJpo(ProductBrief productBrief) {
        /* Autogen by nara studio */
        super(productBrief);
        BeanUtils.copyProperties(productBrief, this);
    }

    public ProductBrief toDomain() {
        /* Autogen by nara studio */
        ProductBrief productBrief = new ProductBrief(getId(), genActorKey());
        BeanUtils.copyProperties(this, productBrief);
        return productBrief;
    }

    public static List<ProductBrief> toDomains(List<ProductBriefJpo> productBriefJpos) {
        /* Autogen by nara studio */
        return productBriefJpos.stream().map(ProductBriefJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static ProductBriefJpo sample() {
        /* Autogen by nara studio */
        return new ProductBriefJpo(ProductBrief.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample());
    }
}
