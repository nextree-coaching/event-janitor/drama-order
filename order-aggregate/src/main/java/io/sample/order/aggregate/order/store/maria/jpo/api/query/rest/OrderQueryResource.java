/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.store.maria.jpo.api.query.rest;

import io.sample.order.aggregate.order.domain.entity.Order;
import io.sample.order.aggregate.order.store.OrderStore;
import io.sample.order.aggregate.order.store.maria.jpo.OrderJpo;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.naraway.accent.util.query.RdbQueryRequest;

import javax.persistence.EntityManager;
import io.naraway.accent.domain.message.api.query.QueryResponse;
import io.sample.order.aggregate.order.store.maria.jpo.api.query.query.OrderQuery;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import io.sample.order.aggregate.order.store.maria.jpo.api.query.query.OrderDynamicQuery;
import java.util.List;
import io.sample.order.aggregate.order.store.maria.jpo.api.query.query.OrdersDynamicQuery;

@RestController
@RequestMapping("/aggregate/order/order/query")
public class OrderQueryResource implements OrderQueryFacade {
    //
    private final OrderStore orderStore;
    private final RdbQueryRequest<OrderJpo> request;

    public OrderQueryResource(OrderStore orderStore, EntityManager entityManager) {
        /* Autogen by nara studio */
        this.orderStore = orderStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QueryResponse<Order> execute(@RequestBody OrderQuery orderQuery) {
        /* Autogen by nara studio */
        orderQuery.execute(orderStore);
        return orderQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-single")
    public QueryResponse<Order> execute(@RequestBody OrderDynamicQuery orderDynamicQuery) {
        /* Autogen by nara studio */
        orderDynamicQuery.execute(request);
        return orderDynamicQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QueryResponse<List<Order>> execute(@RequestBody OrdersDynamicQuery ordersDynamicQuery) {
        /* Autogen by nara studio */
        ordersDynamicQuery.execute(request);
        return ordersDynamicQuery.getQueryResponse();
    }
}
