/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.api.query.query;

import io.sample.order.aggregate.ProductBrief.store.maria.jpo.ProductBriefJpo;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.query.CqrsDynamicQuery;
import io.sample.order.aggregate.ProductBrief.domain.entity.ProductBrief;
import io.naraway.accent.util.query.RdbQueryRequest;
import io.naraway.accent.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class ProductBriefDynamicQuery extends CqrsDynamicQuery<ProductBrief> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<ProductBriefJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), ProductBriefJpo.class);
        TypedQuery<ProductBriefJpo> query = RdbQueryBuilder.build(request);
        ProductBriefJpo productBriefJpo = query.getSingleResult();
        setQueryResult(Optional.ofNullable(productBriefJpo).map(jpo -> jpo.toDomain()).orElse(null));
    }
}
