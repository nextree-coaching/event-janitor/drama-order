/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.store.maria;

import io.sample.order.aggregate.order.domain.entity.Order;
import io.sample.order.aggregate.order.store.OrderStore;
import io.sample.order.aggregate.order.store.maria.repository.OrderMariaRepository;
import org.springframework.stereotype.Repository;
import io.sample.order.aggregate.order.store.maria.jpo.OrderJpo;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import io.naraway.accent.domain.type.Offset;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Repository
public class OrderMariaStore implements OrderStore {
    /* Autogen by nara studio */
    private final OrderMariaRepository orderMariaRepository;

    public OrderMariaStore(OrderMariaRepository orderMariaRepository) {
        /* Autogen by nara studio */
        this.orderMariaRepository = orderMariaRepository;
    }

    @Override
    public void create(Order order) {
        /* Autogen by nara studio */
        OrderJpo orderJpo = new OrderJpo(order);
        orderMariaRepository.save(orderJpo);
    }

    @Override
    public Order retrieve(String id) {
        /* Autogen by nara studio */
        Optional<OrderJpo> orderJpo = orderMariaRepository.findById(id);
        return orderJpo.map(OrderJpo::toDomain).orElse(null);
    }

    @Override
    public void update(Order order) {
        /* Autogen by nara studio */
        OrderJpo orderJpo = new OrderJpo(order);
        orderMariaRepository.save(orderJpo);
    }

    @Override
    public void delete(Order order) {
        /* Autogen by nara studio */
        orderMariaRepository.deleteById(order.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        orderMariaRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return orderMariaRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
