/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.api.query.rest;

import io.naraway.accent.domain.message.api.query.QueryResponse;
import io.sample.order.aggregate.ProductBrief.domain.entity.ProductBrief;
import io.sample.order.aggregate.ProductBrief.api.query.query.ProductBriefQuery;
import io.sample.order.aggregate.ProductBrief.api.query.query.ProductBriefDynamicQuery;
import java.util.List;
import io.sample.order.aggregate.ProductBrief.api.query.query.ProductBriefsDynamicQuery;

public interface ProductBriefQueryFacade {
    /* Autogen by nara studio */
    QueryResponse<ProductBrief> execute(ProductBriefQuery productBriefQuery);
    QueryResponse<ProductBrief> execute(ProductBriefDynamicQuery productBriefDynamicQuery);
    QueryResponse<List<ProductBrief>> execute(ProductBriefsDynamicQuery productBriefsDynamicQuery);
}
