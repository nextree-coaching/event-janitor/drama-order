/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.store.maria.jpo.api.query.query;

import io.sample.order.aggregate.order.domain.entity.Order;
import io.sample.order.aggregate.order.store.maria.jpo.OrderJpo;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.query.CqrsDynamicQuery;
import io.naraway.accent.util.query.RdbQueryRequest;
import io.naraway.accent.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class OrderDynamicQuery extends CqrsDynamicQuery<Order> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<OrderJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), OrderJpo.class);
        TypedQuery<OrderJpo> query = RdbQueryBuilder.build(request);
        OrderJpo orderJpo = query.getSingleResult();
        setQueryResult(Optional.ofNullable(orderJpo).map(jpo -> jpo.toDomain()).orElse(null));
    }
}
