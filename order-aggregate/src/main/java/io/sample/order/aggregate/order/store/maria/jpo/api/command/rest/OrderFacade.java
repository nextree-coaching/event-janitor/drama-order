/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.store.maria.jpo.api.command.rest;

import io.naraway.accent.domain.message.api.command.CommandResponse;
import io.sample.order.aggregate.order.store.maria.jpo.api.command.command.OrderCommand;

public interface OrderFacade {
    /* Autogen by nara studio */
    CommandResponse executeOrder(OrderCommand orderCommand);
}
