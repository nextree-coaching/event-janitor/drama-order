/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.store.maria.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import io.sample.order.aggregate.order.store.maria.jpo.OrderJpo;

public interface OrderMariaRepository extends PagingAndSortingRepository<OrderJpo, String> {
    /* Autogen by nara studio */
}
