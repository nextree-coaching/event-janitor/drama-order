/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.store.maria.jpo.api.query.query;

import io.sample.order.aggregate.order.domain.entity.Order;
import io.sample.order.aggregate.order.store.OrderStore;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.query.CqrsBaseQuery;

@Getter
@Setter
@NoArgsConstructor
public class OrderQuery extends CqrsBaseQuery<Order> {
    /* Autogen by nara studio */
    private String orderId;

    public void execute(OrderStore orderStore) {
        /* Autogen by nara studio */
        setQueryResult(orderStore.retrieve(orderId));
    }
}
