/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.domain.entity.sdo;

import io.naraway.accent.domain.type.IdName;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import io.naraway.accent.util.json.JsonSerializable;
import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductBriefCdo implements JsonSerializable {
    //
    private ActorKey actorKey;
    private IdName idName;
    private String briefView;

    public String toString() {
        //
        return toJson();
    }

    public static ProductBriefCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ProductBriefCdo.class);
    }

    public static ProductBriefCdo sample() {
        //
        return new ProductBriefCdo();
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
