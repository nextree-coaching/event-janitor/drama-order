/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.domain.entity;

import io.naraway.accent.domain.ddd.GenOptions;
import io.naraway.accent.domain.type.IdName;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.ddd.StageEntity;
import io.naraway.accent.domain.ddd.DomainAggregate;
import io.naraway.accent.domain.key.stage.ActorKey;
import io.sample.order.aggregate.order.domain.entity.sdo.OrderCdo;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import org.springframework.beans.BeanUtils;
import io.naraway.accent.domain.type.NameValue;
import java.util.Optional;

/*
ex)
@GenerateOptions(
    updatable = { "name" },
    properties = {
        @RdbProperty(name = "name", columnName = "col_name", columnType = "varchar(255)", columnNullable = true)
    })
*/
@Getter
@Setter
@NoArgsConstructor
@GenOptions(updatable = {"quantity", "canceled"})
public class Order extends StageEntity implements DomainAggregate {
    //
    private IdName product;
    private int quantity;
    private boolean canceled;

    public Order(String id, ActorKey actorKey) {
        //
        super(id, actorKey);
    }

    public Order(OrderCdo orderCdo) {
        //
        super(orderCdo.getActorKey());
        BeanUtils.copyProperties(orderCdo, this);
    }

    public static Order newInstance(OrderCdo orderCdo, NameValueList nameValueList) {
        //
        Order order = new Order(orderCdo);
        order.modifyAttributes(nameValueList);
        return order;
    }

    public String toString() {
        //
        return toJson();
    }

    public static Order fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Order.class);
    }

    public static Order sample() {
        //
        return new Order(OrderCdo.sample());
    }

    @Override
    protected void modifyAttributes(NameValueList nameValues) {
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch(nameValue.getName()) {
                case "quantity":
                    this.quantity = Integer.parseInt(value);
                    break;
                case "canceled":
                    this.canceled = Boolean.parseBoolean(value);
                    break;
                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    public NameValueList genNameValueList() {
        NameValueList nameValueList = NameValueList.newEmptyInstance();
        Optional.ofNullable(this.quantity).ifPresent(value -> nameValueList.add(new NameValue("quantity", value.toString())));
        Optional.ofNullable(this.canceled).ifPresent(value -> nameValueList.add(new NameValue("canceled", JsonUtil.toJson(value))));
        return nameValueList;
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
