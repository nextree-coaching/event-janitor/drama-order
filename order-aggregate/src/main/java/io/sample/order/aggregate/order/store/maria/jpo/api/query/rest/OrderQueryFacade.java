/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.order.store.maria.jpo.api.query.rest;

import io.naraway.accent.domain.message.api.query.QueryResponse;
import io.sample.order.aggregate.order.domain.entity.Order;
import io.sample.order.aggregate.order.store.maria.jpo.api.query.query.OrderQuery;
import io.sample.order.aggregate.order.store.maria.jpo.api.query.query.OrderDynamicQuery;
import java.util.List;
import io.sample.order.aggregate.order.store.maria.jpo.api.query.query.OrdersDynamicQuery;

public interface OrderQueryFacade {
    /* Autogen by nara studio */
    QueryResponse<Order> execute(OrderQuery orderQuery);
    QueryResponse<Order> execute(OrderDynamicQuery orderDynamicQuery);
    QueryResponse<List<Order>> execute(OrdersDynamicQuery ordersDynamicQuery);
}
