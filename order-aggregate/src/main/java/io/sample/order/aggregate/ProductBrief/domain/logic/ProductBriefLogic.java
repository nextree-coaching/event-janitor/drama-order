/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.order.aggregate.ProductBrief.domain.logic;

import io.sample.order.aggregate.ProductBrief.domain.event.ProductBriefEvent;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import io.sample.order.aggregate.ProductBrief.store.ProductBriefStore;
import io.naraway.janitor.EventStream;
import io.sample.order.aggregate.ProductBrief.api.command.command.ProductBriefCommand;
import io.naraway.accent.domain.message.api.command.CommandResponse;
import io.naraway.accent.domain.message.api.FailureMessage;
import java.util.Optional;
import io.sample.order.aggregate.ProductBrief.domain.entity.sdo.ProductBriefCdo;
import io.naraway.accent.domain.type.NameValueList;
import java.util.List;
import java.util.stream.Collectors;
import io.sample.order.aggregate.ProductBrief.domain.entity.ProductBrief;
import java.util.NoSuchElementException;

@Service
@Transactional
public class ProductBriefLogic {
    //
    private final ProductBriefStore productBriefStore;
    private final EventStream eventStream;

    public ProductBriefLogic(ProductBriefStore productBriefStore, EventStream eventStream) {
        /* Autogen by nara studio */
        this.productBriefStore = productBriefStore;
        this.eventStream = eventStream;
    }

    public ProductBriefCommand routeCommand(ProductBriefCommand command) {
        /* Autogen by nara studio */
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = this.registerProductBriefs(command.getProductBriefCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = Optional.ofNullable(command.getNameValues()).filter(nameValueList -> command.getNameValues().size() != 0).map(nameValueList -> this.registerProductBrief(command.getProductBriefCdo(), nameValueList)).orElse(this.registerProductBrief(command.getProductBriefCdo()));
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyProductBrief(command.getProductBriefId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getProductBriefId()));
                break;
            case Remove:
                this.removeProductBrief(command.getProductBriefId());
                command.setCommandResponse(new CommandResponse(command.getProductBriefId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerProductBrief(ProductBriefCdo productBriefCdo) {
        /* Autogen by nara studio */
        ProductBrief productBrief = new ProductBrief(productBriefCdo);
        if (productBriefStore.exists(productBrief.getId())) {
            throw new IllegalArgumentException("productBrief already exists. " + productBrief.getId());
        }
        productBriefStore.create(productBrief);
        ProductBriefEvent productBriefEvent = ProductBriefEvent.newProductBriefRegisteredEvent(productBrief, productBrief.getId());
        eventStream.publishEvent(productBriefEvent);
        return productBrief.getId();
    }

    public String registerProductBrief(ProductBriefCdo productBriefCdo, NameValueList nameValueList) {
        /* Autogen by nara studio */
        ProductBrief productBrief = ProductBrief.newInstance(productBriefCdo, nameValueList);
        if (productBriefStore.exists(productBrief.getId())) {
            throw new IllegalArgumentException("productBrief already exists. " + productBrief.getId());
        }
        productBriefStore.create(productBrief);
        ProductBriefEvent productBriefEvent = ProductBriefEvent.newProductBriefRegisteredEvent(productBrief, productBrief.getId());
        eventStream.publishEvent(productBriefEvent);
        return productBrief.getId();
    }

    public List<String> registerProductBriefs(List<ProductBriefCdo> productBriefCdos) {
        /* Autogen by nara studio */
        return productBriefCdos.stream().map(productBriefCdo -> this.registerProductBrief(productBriefCdo)).collect(Collectors.toList());
    }

    public ProductBrief findProductBrief(String productBriefId) {
        /* Autogen by nara studio */
        ProductBrief productBrief = productBriefStore.retrieve(productBriefId);
        if (productBrief == null) {
            throw new NoSuchElementException("ProductBrief id: " + productBriefId);
        }
        return productBrief;
    }

    public void modifyProductBrief(String productBriefId, NameValueList nameValues) {
        /* Autogen by nara studio */
        ProductBrief productBrief = findProductBrief(productBriefId);
        productBrief.modify(nameValues);
        productBriefStore.update(productBrief);
        ProductBriefEvent productBriefEvent = ProductBriefEvent.newProductBriefModifiedEvent(productBriefId, nameValues, productBrief);
        eventStream.publishEvent(productBriefEvent);
    }

    public void removeProductBrief(String productBriefId) {
        /* Autogen by nara studio */
        ProductBrief productBrief = findProductBrief(productBriefId);
        productBriefStore.delete(productBrief);
        ProductBriefEvent productBriefEvent = ProductBriefEvent.newProductBriefRemovedEvent(productBrief, productBrief.getId());
        eventStream.publishEvent(productBriefEvent);
    }

    public boolean existsProductBrief(String productBriefId) {
        /* Autogen by nara studio */
        return productBriefStore.exists(productBriefId);
    }

    public void handleEventForProjection(ProductBriefEvent productBriefEvent) {
        /* Autogen by nara studio */
        switch(productBriefEvent.getCqrsDataEventType()) {
            case Registered:
                productBriefStore.create(productBriefEvent.getProductBrief());
                break;
            case Modified:
                ProductBrief productBrief = productBriefStore.retrieve(productBriefEvent.getProductBriefId());
                productBrief.modify(productBriefEvent.getNameValues());
                productBriefStore.update(productBrief);
                break;
            case Removed:
                productBriefStore.delete(productBriefEvent.getProductBriefId());
                break;
        }
    }
}
