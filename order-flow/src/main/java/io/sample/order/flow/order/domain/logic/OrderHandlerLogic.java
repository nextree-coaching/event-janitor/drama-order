package io.sample.order.flow.order.domain.logic;

import io.naraway.janitor.EventStream;
import io.sample.order.aggregate.order.domain.event.OrderEvent;
import io.sample.order.aggregate.order.domain.logic.OrderLogic;
import io.sample.order.event.order.ProductSoldEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class OrderHandlerLogic {
    //
    private final EventStream eventStream;
    private final OrderLogic orderLogic;

    public void onOrder(OrderEvent orderEvent) {
        //
        switch (orderEvent.getCqrsDataEventType()) {
            case Registered :
                eventStream.publishEvent(new ProductSoldEvent(orderEvent.getOrderId(),
                        orderEvent.getOrder().getProduct().getId(),
                        orderEvent.getOrder().getQuantity()));
                return;
            default:
                throw new IllegalArgumentException(orderEvent.getCqrsDataEventType()
                        + "is not allowed in OrderHandlerLogic:onOrder");
        }
    }
}
