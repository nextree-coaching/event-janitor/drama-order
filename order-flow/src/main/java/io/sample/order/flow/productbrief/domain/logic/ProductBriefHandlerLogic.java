package io.sample.order.flow.productbrief.domain.logic;

import io.naraway.accent.domain.type.IdName;
import io.sample.order.aggregate.ProductBrief.domain.entity.sdo.ProductBriefCdo;
import io.sample.order.aggregate.ProductBrief.domain.logic.ProductBriefLogic;
import io.naraway.product.event.product.ProductDomainEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductBriefHandlerLogic {
    //
    private final ProductBriefLogic productBriefLogic;
//    private final ProductBriefUserLogic productBriefUserLogic;

    public void onProduct(ProductDomainEvent event) {
        //
        log.debug("onProductEvent : \n{}", event.toString());
        switch (event.getEventType()) {
            case Registered:
                newProductBrief(event);
                break;
            case Modified:
                break;
            case Removed:
                break;
        }
    }

    private void newProductBrief(ProductDomainEvent event) {
        //
        String overView = String.format("%s / %s / %s / %s",
                event.getCategory(),
                event.getSize(),
                event.getMaterial(),
                event.getColor());
        ProductBriefCdo cdo = new ProductBriefCdo(event.getActorKey(),
                IdName.of(event.getId(), event.getName()),
                overView);

        productBriefLogic.registerProductBrief(cdo);
    }
}
