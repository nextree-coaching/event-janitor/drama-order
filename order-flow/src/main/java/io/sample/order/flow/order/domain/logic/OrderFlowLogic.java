package io.sample.order.flow.order.domain.logic;

import io.naraway.accent.domain.message.api.command.CommandResponse;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.janitor.EventStream;
import io.sample.order.aggregate.ProductBrief.domain.logic.ProductBriefUserLogic;
import io.sample.order.aggregate.order.domain.entity.Order;
import io.sample.order.aggregate.order.domain.entity.sdo.OrderCdo;
import io.sample.order.aggregate.order.domain.logic.OrderLogic;
import io.sample.order.aggregate.order.store.maria.jpo.api.command.command.OrderCommand;
import io.sample.order.event.order.OrderCanceledEvent;
import io.sample.order.event.order.ProductSoldEvent;
import io.sample.order.flow.order.api.command.CancelOrderCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class OrderFlowLogic {
    //
    private final OrderLogic orderLogic;
    private final ProductBriefUserLogic productBriefUserLogic;
    private final EventStream eventStreamService;

    public CommandResponse order(OrderCommand command) {
        //
        OrderCdo cdo = command.getOrderCdo();
        String orderId = orderLogic.registerOrder(cdo);
        productBriefUserLogic.decreaseStock(cdo.getProduct().getId(), cdo.getQuantity());

        ProductSoldEvent event = new ProductSoldEvent(orderId,
                cdo.getProduct().getId(),
                cdo.getQuantity());
        eventStreamService.publishEvent(event);

        return new CommandResponse();
    }

    public CommandResponse cancelOrder(CancelOrderCommand command) {
        //
        Order order = orderLogic.findOrder(command.getOrderId());
        NameValueList nameValues = NameValueList.newInstance("canceled", "true");
        orderLogic.modifyOrder(command.getOrderId(), nameValues);
        productBriefUserLogic.increaseStock(order.getProduct().getId(), order.getQuantity());

        OrderCanceledEvent event = new OrderCanceledEvent(order.getId(),
                order.getProduct().getId(),
                order.getQuantity());
        eventStreamService.publishEvent(event);

        return new CommandResponse();
    }

}
